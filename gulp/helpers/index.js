module.exports = function (config) {
    'use strict';
    var gulp = require('gulp');

    var autoprefixer = require('gulp-autoprefixer'),
        stylus = require('gulp-stylus'),
        cssmin = require('gulp-cssmin'),
        nunjucks = require('gulp-nunjucks'),

        webserver = require('gulp-webserver'),
        watch = require('gulp-watch'),
        gulpsync = require('gulp-sync')(gulp),
        gulpif = require('gulp-if'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        requi = require('gulp-requi'),
        rename = require('gulp-rename');

    var errors = require('./errors');

// =================== CSS ===================
    gulp.task('css', function () {
        gulp.src(config.css.source)
            .pipe(requi())
            .pipe(gulpif(/[.]styl$/, stylus()))
            .on('error', errors)
            .pipe(autoprefixer({browsers: config.css.autoprefixer}))
            .pipe(concat(config.css.name))
            .pipe(gulp.dest(config.css.dist))

            .pipe(rename(function (path) {
                path.basename += '.min';
            }))
            .pipe(cssmin())
            .pipe(gulp.dest(config.css.dist));
    });


// =================== Scripts ===================
// TODO : библоитеки и плагины должны переносится из app в dist, собираются только scripts
    gulp.task('js-scripts', function () {
        gulp.src(config.js.source.scripts)
            //.pipe(rename(function (path) {
            //    path.basename += '.min';
            //}))
            //.pipe(uglify())
            .pipe(gulp.dest(config.js.dist.scripts));
    });

    gulp.task('js-plugins', function () {
        gulp.src(config.js.source.plugins)
            //.pipe(rename(function (path) {
            //    path.basename += '.min';
            //}))
            //.pipe(uglify())
            .pipe(gulp.dest(config.js.dist.plugins));
    });

    gulp.task('js-conf', function () {
        gulp.src(config.js.source.conf)
            //.pipe(rename(function (path) {
            //    path.basename += '.min';
            //}))
            //.pipe(uglify())
            .pipe(gulp.dest(config.js.dist.conf));
    });


// ==================== Templates ====================
    gulp.task('html', function () {
        gulp.src(config.html.source)
            // .pipe(gulpif(/[.]nunjucks$/, nunjucks()))
            .pipe(nunjucks())
            // .pipe(rev({assetsDir: './dist'}))
            .pipe(gulp.dest(config.html.dist));
    });

// =================== PNG Sprites ===================
  // gulp.task('baseSprites', function () {

  //   var opts = {
  //     cssTemplate: config.sprites_png.tmpl,
  //     imgName: config.sprites_png.nameSprite,
  //     cssName: config.sprites_png.nameMixins,
  //     imgPath: config.sprites_png.imgPath,
  //     padding: 0,
  //     cssFormat: 'stylus',
  //     cssVarMap: function (sprite) {
  //       sprite.prefix = config.sprites_png.prefixMixin;
  //       if (config.sprites_png.supportRetina) {
  //         sprite.retina = true;
  //         sprite.retinaImgPath = config.sprites_png.retinaImgPath;
  //       } else {
  //         sprite.retina = false;
  //       }
  //     }
  //   };

  //   var spriteData = gulp.src(config.sprites_png.source);

  //   spriteData = spriteData.pipe(
  //     spritesmith(opts)
  //   );

  //   spriteData.css.pipe(gulp.dest(config.sprites_png.mixins));
  //   spriteData.img.pipe(gulp.dest(config.sprites_png.dest));
  // });

  // gulp.task('retinaSprites', function () {

  //   var opts = {
  //     imgName: config.sprites_png.retinaNameSprite,
  //     imgPath: config.sprites_png.retinaImgPath,
  //     padding: 0,
  //     cssName: '.'
  //   };

  //   var spriteData = gulp.src(config.sprites_png.retinaSource);

  //   spriteData = spriteData.pipe(
  //     spritesmith(opts)
  //   );

  //   spriteData.img.pipe(gulp.dest(config.sprites_png.dest));
  // });

  // var tasksSprites = ['baseSprites'];

  // if (config.sprites_png.supportRetina) {
  //   tasksSprites.push('retinaSprites');
  // }

  // gulp.task('sprites', tasksSprites);


// =================== SVG Sprites ===================
  // gulp.task('svgSprites', function () {
  //   gulp
  //     .src(config.sprites_svg.source)
  //     .pipe(svgmin())
  //     .pipe(svgstore())
  //     .pipe(gulp.dest(config.sprites_svg.dest));
  // });

// ===================  jade  ===================

  // gulp.task('jade', function () {
  //   gulp.src(config.jade.source)
  //       .pipe(jade({pretty: true}))
  //       .on('error', errors)
            // .pipe(rev())
  //       .pipe(gulp.dest(config.jade.dest));
  // });


// ===================  webserver  ==============
    gulp.task('webserver', function () {
        gulp.src(config.webserver.src)
            .pipe(webserver(config.webserver.options));
    });


// =================== watch ====================
    gulp.task('watch', function () {
        watch(config.css.watch, function () {
            gulp.start('css');
        });

        watch(config.js.watch.scripts, function () {
            gulp.start('js-scripts');
        });

        watch(config.js.watch.plugins, function () {
            gulp.start('js-plugins');
        });

        watch(config.js.watch.conf, function () {
            gulp.start('js-conf');
        });

        watch(config.html.watch, function () {
            gulp.start('html');
        });

        // watch(config.sprites_svg.watch, function () {
        //   gulp.start('svgSprites');
        //   // gulp.start('jade');
        // });


        // if (config.sprites_png.enable) {
        //   watch(config.sprites_png.watch, function () {
        //     gulp.start('sprites');
        //   });
        // }

    });

    var tasks = [
        'css',
        ['js-scripts', 'js-plugins', 'js-conf'],
        'html'
        // 'svgSprites',
    ];

    // if (config.sprites_png.enable) {
    //   tasks.push('sprites');
    // }

    if (config.webserver.enable) {
        tasks.push('webserver');
    }

    gulp.task('default', gulpsync.async(tasks));

    gulp.task('start', function () {
        console.log('<=============== [start mode] =============== >');
        gulp.start(gulpsync.sync(['default', 'watch']));
    });
};