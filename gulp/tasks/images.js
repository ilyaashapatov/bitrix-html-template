var config = require('../config').images,
    gulp = require('gulp');

var errors = require('../helpers/errors');

// images plugins
var imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

gulp.task('images', function () {
    'use strict';

    return gulp.src(config.src)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .on('error', errors)
        .pipe(gulp.dest(config.dest));
});