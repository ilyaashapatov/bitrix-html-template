var config = require('../config').html,
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    gulpIf = require('gulp-if'),
    argv = require('yargs').argv;

var errors = require('../helpers/errors');

// html plugins
var twig = require('gulp-twig'),
    htmlmin = require('gulp-htmlmin');


// Minify (Для минификации запускать сборку с флагом: --minify)
// Пример: $ gulp html --minify
gulp.task('html', function () {
    'use strict';
    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(twig())
        .on('error', errors)
        // .pipe(gulpIf(argv.minify, htmlmin({
        //     collapseWhitespace: true,
        //     removeComments: true
        // })))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true}));
});