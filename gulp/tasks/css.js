var config = require('../config').css,
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    gulpIf = require('gulp-if'),
    concat = require('gulp-concat'),
    requi = require('gulp-requi'),
    argv = require('yargs').argv;

var errors = require('../helpers/errors');

// CSS & plugins
var stylus = require('gulp-stylus'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano');


// Minify (Для минификации запускать сборку с флагом: --minify)
// Пример: $ gulp css --minify
gulp.task('css', function () {
    'use strict';

    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(requi())
        .pipe(gulpIf(/[.]styl$/, stylus()))
        .on('error', errors)
        .pipe(autoprefixer({browsers: config.prefix}))
        .pipe(concat(config.name))
        .pipe(gulpIf(argv.minify, cssnano({discardComments: {removeAll: true}})))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true}));
});