var path = require('path');

var src = '../src/',
    dist = 'dist/';

module.exports = {
    css: {
        src: path.join(__dirname, src + 'css/index.styl'),
        prefix: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera > 10', 'Explorer >= 9'],
        name: 'style.css',
        dest: dist + 'css/'
    },

    images: {
        src: path.join(__dirname, src + 'images/**/*'),
        dest: dist + 'images/'
    },

    html: {
        src: path.join(__dirname, src + '*.twig'),
        dest: dist
    }//,

    // bower: {
    //     target: 'vendor.js',
    //     dest: jsPath
    // },





    // js: {
    //     src: path.join(__dirname, '../src/js/index.js'),
    //     target: 'scripts.js',
    //     dest: jsPath
    // }
};

// var config = {

//     // build js
//     js: {
//         source: {
//             scripts: appPath + 'js/scripts/**.*',
//             plugins: appPath + 'js/plugins/**.*',
//             conf: appPath + 'js/app.js'
//         },
//         dist: {
//             scripts: dist + 'js/scripts',
//             plugins: dist + 'js/plugins',
//             conf: dist + 'js/'
//         },
//         watch: {
//             scripts: appPath + 'js/scripts/**',
//             plugins: appPath + 'js/plugins/**',
//             conf: appPath + 'js/app.js'
//         }
//     },

//     html: {
//         source: [appPath + 'templates/**', '!' + appPath + 'templates/includes/*', '!' + appPath + 'templates/base.*'],
//         dist: dist,
//         watch: appPath + 'templates/**'
//     },

//     // webserver config
//     webserver: {
//         enable: true,
//         src: 'dist/',
//         options: {
//             port: 8001
//         }
//     },




//   // build png sprite
//     sprites_png: {
//         enable: false,
//         tmpl: 'utils/sprite_png-template.handlebars',

//         // files for watch
//         source: [appPath + 'sprites/*.png', '!' + appPath + 'sprites/*@*.png'],
//         watch: appPath + 'sprites/*.png',

//         // create mixin config
//         mixins: appPath + 'css/variables/',
//         nameMixins: 'sprite-mixins.styl',
//         prefixMixin: 's-',

//         // create sprite config
//         nameSprite: 'sprite.png',
//         dist: dist + 'img/',
//         imgPath: '../img/sprite.png',

//         // support retina config
//         supportRetina: false,
//         retinaSource: appPath + 'sprites/*@2x.png',
//         retinaNameSprite: 'sprite@2x.png',
//         retinaImgPath: '../img/sprite@2x.png'
//     }

//   // // build svg sprite
//   // sprites_svg: {
//   //   enable: true,
//   //   source: source + '/sprites_svg/*.svg',
//   //   watch: source + '/sprites_svg/*.svg',
//   //   dist: templates + '/includes/'
//   // },
// };

// require('./utils/index')(config);