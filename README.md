Шаблон для быстрого старта верстки проекта под битрикс (Gulp)
* Gulp
* Nunjacks (html)
* Stylus
* JavaScript
* SVG

=========================================

В качестве сборщика проекта используется `gulp`

## Требования к установке

* [Node.js 0.10+](http://nodejs.org) – это программная платформа, основанная на языке JavaScript и позволяющая легко создавать быстрые и масштабируемые сетевые приложения.
* [Git Bash](http://msysgit.github.io/) – для пользователей операционной системы Windows.


## Старт проекта

### Установка

Клонируем репозиторий и устанавливаем все необходимые зависимости:

```bash
git clone https://github.com/bem/project-stub.git my-project
cd my-project
npm install
```

### Запуск

Существует несколько основных команд при работе со сборщиком:

* `gulp` – Cборка проекта без запуска сервера. Дополнительно запускается процесс отслеживания изменений.
* `gulp start` – Сборка проекта без с запуском сервера и процессом отслеживания изменений. Доступ из браузера по адресу http://localhost:8001/index.html
* `gulp zip` – упаковка содержимого папки `dist` в архив. Архив кладется в корень проекта
* `gulp deploy` – заливает содержимое папки `dist` на сервер по ftp


## Что может этот шаблон

### Сборка спрайтов

#### Настрока конфигурации
Предусмотрено создание спрайтов в проекте, включение и отключение производится в конфиге `gulpfile.js` (по умолчанию отключено):

```json
    sprites: {
      enable: false,
      // code ...
  }
```

Так же предусмотренно создание спрайтов и для retina-экранов. Обратите внимание на следующие настройки (`gulpfile.js`):
по умолчанию эта опция отключена

```json
    sprites: {
        // code ...
        supportRetina: false,
    }
```

#### Как работать со спрайтами?

Создем иконку с названием, например `home.png` и кладем ее в папку `build/sprites`.
Сборщик добавляет в файл `build/css/varibles/sprite-mixin.styl` миксин со стилями:

```css
  s-home(arg = null, arg2 = null)
    importance = unquote("")
    if arg and arg2
      if arg2 == !important or arg2 == important
        importance = !important
    else if arg == !important or arg == important
        importance = !important
    if p in arg or r in arg
      background-position 0px 0px importance
      if r in arg
        width 38px importance
        height 38px importance
    else
      display inline-block importance
      background url('../img/sprite.png') no-repeat 0px 0px importance
      width 38px importanc
      height 38px importance

      @media $m-retina
        background-size 76px 38px
        background-image url('../img/sprite@2x.png')
```

и обновляет файл `dist/img/sprite.png`

Если файлов `sprite-mixin.styl` и `sprite.png` не существует, то сборщик создаст их.

Далее, используя созданный миксин, в css коде необходимо прописать только `s-home()` у класса с иконкой.
Название миксина берется из названия иконки и в начало добавляется префикс `s-`

### SVG иконки

Coming soon..


### Работа с html шаблонами

В проекте есть возможность работать как с Twig шаблонами, так и с простыми html файлами.
Верстка в Twig практически ничем на отличается от верстки обычным html, за исключением использования отпределнных template тегов.
Подробнее изучить синтаксис Twig можно на [официальном сайте](http://twig.sensiolabs.org/)


Все шаблоны должны лежать в папке `build/templates`. Компилируются все файлы, кроме:
-  содержимое папок `includes`
- `base.twig`/`base.html`



[1]: <https://github.com/trolev/html-template-2x2>  "HTML-template 2x2"
